<?php

/**
 * @file
 * Theme related functions for processing our output style plugins.
 *
 * Views bug: http://drupal.org/node/593336
 */


/**
 * Theme a status message
 */
function theme_big_dipper_message($var) {
  $output = '';
  $output .= '<div class="messages status ' . $var['type'] . '">';
  $output .= $var['message'];
  $output .= '</div>';
  return $output;
}

/**
 * Theme a feed link.
 *
 * This theme function uses the theme pattern system to allow it to be
 * overidden in a more specific manner. The options for overiding this include
 * providing per display id; per type; per display id and per type.
 *
 * e.g.
 * For the view "export_test" with the display "page_1" and the type "csv" you
 * would have the following options.
 *   big_dipper_feed_icon__export_test__page_1__csv
 *   big_dipper_feed_icon__export_test__page_1
 *   big_dipper_feed_icon__export_test__csv
 *   big_dipper_feed_icon__page_1__csv
 *   big_dipper_feed_icon__page_1
 *   big_dipper_feed_icon__csv
 *   big_dipper_feed_icon
 *
 * @ingroup themeable
 */
function theme_big_dipper_feed_icon($variables) {
  extract($variables, EXTR_SKIP);
  $url_options = array('html' => true);
  if ($query) {
    $url_options['query'] = $query;
  }
  $image = theme('image', array('path' => $image_path, 'alt' => $text, 'title' => $text));
  return l($image, $url, $url_options);
  
  
}

/**
 * Theme callback for the export complete page.
 *
 * @param $file
 *  Link to output file
 */
 
 
// Use this to create a zip archive from the various tmp folders


function folderzip($source, $destination)
{
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            if (is_dir($file) === true)
            {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}


function theme_big_dipper_complete_page($variables) {
	

//empty the temporary folders used to assemble zip packages
//these must be created beforehand in the files folder with 0777 permissions
//calls to splitter and other package assembly xproc routines must use these folders
	

  if (is_dir('/var/www/mediation/sites/default/files/ditatmp/')) {
    $dir = '/var/www/mediation/sites/default/files/ditatmp/';
    foreach(glob($dir.'*.*') as $v){
        unlink($v);
    }
  }
  
  if (is_dir('/var/www/mediation/sites/default/files/scormtmp/')) {
    $dir = '/var/www/mediation/sites/default/files/scormtmp/';
    foreach(glob($dir.'*.*') as $v){
        unlink($v);
    }
  }
  
  if (is_dir('/var/www/mediation/sites/default/files/epubtmp/META-INF/')) {
    $dir = '/var/www/mediation/sites/default/files/epubtmp/META-INF/';
    foreach(glob($dir.'*.*') as $v){
        unlink($v);
    }
  }
  
  if (is_dir('/var/www/mediation/sites/default/files/epubtmp/OEBPS/')) {
    $dir = '/var/www/mediation/sites/default/files/epubtmp/OEBPS/';
    foreach(glob($dir.'*.*') as $v){
        unlink($v);
    }
  }
  
  if (is_dir('/var/www/mediation/sites/default/files/epubtmp/')) {
    $dir = '/var/www/mediation/sites/default/files/epubtmp/';
    foreach(glob($dir.'*.*') as $v){
        unlink($v);
    }
  }
  
  if (is_dir('/var/www/mediation/sites/default/files/odttmp/')) {
    $dir = '/var/www/mediation/sites/default/files/odttmp/';
    foreach(glob($dir.'*.*') as $v){
        unlink($v);
    }
  }	

	


  
  extract($variables, EXTR_SKIP);
  drupal_set_title(t('Data export successful'));

  $servlex_dita='http://localhost:8080/servlex/transformer/pinboard2dita?who=';
  $servlex_docbook='http://localhost:8080/servlex/transformer/pinboard2docbook?who=';
  $servlex_epub='http://localhost:8080/servlex/transformer/pinboard2epub?who=';
  $servlex_odt='http://localhost:8080/servlex/transformer/pinboard2odt?who=';
  $servlex_scorm='http://localhost:8080/servlex/transformer/pinboard2scorm?who=';
  

  if (!is_dir('/var/www/mediation/sites/default/files/ditatmp')) {
    unlink('/var/www/mediation/sites/default/files/ditatmp');
  }    
  if (!is_dir('/var/www/mediation/sites/default/files/docbooktmp')) {
    unlink('/var/www/mediation/sites/default/files/docbooktmp');
  }
  if (!is_dir('/var/www/mediation/sites/default/files/epubtmp')) {
    unlink('/var/www/mediation/sites/default/files/epubtmp');
  } 
  if (!is_dir('/var/www/mediation/sites/default/files/epubtmp/META-INF')) {
    unlink('/var/www/mediation/sites/default/files/epubtmp/META-INF');
  }  
  if (!is_dir('/var/www/mediation/sites/default/files/epubtmp/OEBPS')) {
    unlink('/var/www/mediation/sites/default/files/epubtmp/OEBPS');
  }    
  if (!is_dir('/var/www/mediation/sites/default/files/odttmp')) {
    unlink('/var/www/mediation/sites/default/files/odttmp');
  }    
  if (!is_dir('/var/www/mediation/sites/default/files/scormtmp')) {
    unlink('/var/www/mediation/sites/default/files/scormtmp');  
  }


  
  //This is the URL of the XML (XHTML) Pinboard output file
  $docfile='http://mediation'.$file;
  
  // this gets the generated file and copies the UTF-8-ified version of the file to a temporary location
  //file_put_contents('/var/www/mediation/sites/default/files/temp.xml',html_entity_decode(file_get_contents($docfile),ENT_QUOTES,'UTF-8'));
  file_put_contents('/var/www/mediation/sites/default/files/temp.xml',file_get_contents($docfile));
  // assigns the address of the temporary file to a variable so that is can be fed to Servlex
  $file = '/var/www/mediation/sites/default/files/temp.xml';
  
  //Servlex needs the reserved characters in the URL query portion hex-encoded
  $file = preg_replace('/(\/)/', '%2F',$file);
  $file = preg_replace('/(:)/', '%3A',$file);
  $file = preg_replace('/(\?)/', '%3F',$file);
  $file = preg_replace('/(\=)/', '%3D',$file);
  $file = preg_replace('/(\&)/', '%26',$file);
  
  // *[1] this is the stem of the saved file's name. The file extension is set in the xproc pipeline at the moment - should be passed as a parameter to the pipeline
  $filestem_dita = $file.'&filename=dita';
  $filestem_docbook = $file.'&filename=docbook';
  $filestem_epub = $file.'&filename=epub';
  $filestem_odt = $file.'&filename=content';
  $filestem_scorm = $file.'&filename=scorm';

  // *[2] This creates an appropriate URL to feed to Servlex - triggering the appropriate transformation at *[4]
  $file_dita = $servlex_dita . $filestem_dita;
  $file_docbook = $servlex_docbook . $filestem_docbook;
  $file_epub = $servlex_epub . $filestem_epub;
  $file_odt = $servlex_odt . $filestem_odt;
  $file_scorm = $servlex_scorm . $filestem_scorm;
  
  // *[3] This is where we will store the transformed DITA and ODT files - this should map to the filelocation variable in the transformer.xproc file in the Servlex transformer package
  $ditafilelocation = '/sites/default/files/dita.dita';
  $docbookfilelocation = '/sites/default/files/docbook.docbook';
  $epubfilelocation = '/sites/default/files/epub.epub';
  $contentxmlfilelocation = '/sites/default/files/content.xml';
  $odtfilelocation = '/sites/default/files/pinboard.odt';
  $scormfilelocation = '/sites/default/files/scorm.scorm';

  // ODT file location expressed relative to scripts folder so that pyconvertor can run PDF/doc conversion etc
  $rel_odtfilelocation = 'pinboard.odt';
  
  // *[4] this sends a request to the Servlex server. This triggers the servlex transformation which saves the resulting document at the location specified in *[3]
  $transformedfile_dita = file_get_contents($file_dita);
  $transformedfile_docbook = file_get_contents($file_docbook);
  $transformedfile_epub = file_get_contents($file_epub);
  $transformedfile_odt = file_get_contents($file_odt);
  $transformedfile_scorm = file_get_contents($file_scorm);
   
  // Below we copy the relevant ODT template file to create the downloadable file (at some point we'll need to do something about the images!)
  
  // Delete any previous file if it exists
  if (file_exists('/var/www/mediation/sites/default/files/pinboard.odt')) {
    unlink('/var/www/mediation/sites/default/files/pinboard.odt');
  }

  // copy the template
  copy("/var/www/mediation/sites/default/files/template-A4.odt","/var/www/mediation/sites/default/files/pinboard.odt");
  
  // copy the template
  copy("/var/www/mediation/sites/default/files/container.xml","/var/www/mediation/sites/default/files/epubtmp/META-INF/container.xml");
  

  
   // zip up the content.xml file into the ODT (note the 'content' is set in *[1])

   $zip = new ZipArchive();
   $zip->open("/var/www/mediation/sites/default/files/pinboard.odt");
   $zip->addFile('/var/www/mediation/sites/default/files/content.xml','content.xml');
   $zip->close();
   
  
   // zip up the dita file
   // Delete any previous zip archive if it exists (otherwise folderzip will add the new files to it)
   if (file_exists('/var/www/mediation/sites/default/files/dita.zip')) {
      unlink('/var/www/mediation/sites/default/files/dita.zip');
   }
   folderzip('/var/www/mediation/sites/default/files/ditatmp/', '/var/www/mediation/sites/default/files/dita.zip');
   
   // zip up the odt file
   // Delete any previous zip archive if it exists (otherwise folderzip will add the new files to it)
   if (file_exists('/var/www/mediation/sites/default/files/odt.zip')) {
      unlink('/var/www/mediation/sites/default/files/odt.zip');
   }
   folderzip('/var/www/mediation/sites/default/files/odttmp/', '/var/www/mediation/sites/default/files/odt.zip');
   
   // zip up the docbook file
   // Delete any previous zip archive if it exists (otherwise folderzip will add the new files to it)
   if (file_exists('/var/www/mediation/sites/default/files/docbook.zip')) {
      unlink('/var/www/mediation/sites/default/files/docbook.zip');
   }
   folderzip('/var/www/mediation/sites/default/files/docbooktmp/', '/var/www/mediation/sites/default/files/docbook.zip');
   
   // zip up the epub file
   // Delete any previous zip archive if it exists (otherwise folderzip will add the new files to it)
   if (file_exists('/var/www/mediation/sites/default/files/epub.zip')) {
      unlink('/var/www/mediation/sites/default/files/epub.zip');
   }
   folderzip('/var/www/mediation/sites/default/files/epubtmp/', '/var/www/mediation/sites/default/files/pinboard.epub');   
   
   
   // zip up the scorm file
   // Delete any previous zip archive if it exists (otherwise folderzip will add the new files to it)
   if (file_exists('/var/www/mediation/sites/default/files/scorm.zip')) {
      unlink('/var/www/mediation/sites/default/files/scorm.zip');
   }
   folderzip('/var/www/mediation/sites/default/files/scormtmp/', '/var/www/mediation/sites/default/files/scorm.zip');

   

  
  $output = '<p>';
  $output .= t('<a href="@link">Download the XHTML version of the document</a>', array('@link' => $docfile));
  $output .= '</p>';
  $output .= '<p>';
  $output .= t('<a href="@link">View the DITA version of the document</a>', array('@link' => $ditafilelocation));
  $output .= '</p>';
  $output .= '<p>';
  $output .= t('<a href="@link">View the DOCBOOK version of the document</a>', array('@link' => $docbookfilelocation));
  $output .= '</p>';
  $output .= '<p>';
  $output .= t('<a href="@link">Download the EPUB version of the document</a>', array('@link' => $epubfilelocation));
  $output .= '</p>';
  $output .= '<p>';
  $output .= '<p>';
  $output .= t('<a href="@link">View the LibreOffice/OpenOffice content.xml file</a>', array('@link' => $contentxmlfilelocation));
  $output .= '</p>';
  $output .= '<p>';
  $output .= t('<a href="@link">Download the LibreOffice/OpenOffice version of the document</a>', array('@link' => $odtfilelocation));
  $output .= '</p>';
  $output .= '<p>';


  // below runs the OOo python converter when headless LibreOffice is running
  // Command to start libreoffice asa service is 
  // libreoffice --nofirststartwizard --accept="socket,host=localhost,port=8100;urp;StarOffice.Service"
  // or headless
  // libreoffice --headless --nofirststartwizard --accept="socket,host=localhost,port=8100;urp;StarOffice.Service"
  
  // $odt2doc='python /var/www/mediation/sites/default/scripts/docconv.py /var/www/mediation/sites/default/files/pinboard.odt /var/www/mediation/sites/default/files/pinboard.doc';
  
  // $odt2pdf='python /var/www/mediation/sites/default/scripts/docconv.py /var/www/mediation/sites/default/files/pinboard.odt /var/www/mediation/sites/default/files/pinboard.pdf';

  // $result = exec($odt2doc);
  
  // $result = exec($odt2pdf);

  // $output .= '<p>';
  // $output .= t('<a href="@link">Download the PDF version of the document</a>', array('@link' => '/sites/default/files/pinboard.pdf'));
  // $output .= '</p>';
  
  // $output .= '<p>';
  // $output .= t('<a href="@link">Download the DOC version of the document</a>', array('@link' => '/sites/default/files/pinboard.doc'));
  // $output .= '</p>'
  
  $output .= '<p>';
  $output .= t('<a href="@link">Download the EPUB zip file</a>', array('@link' => '/sites/default/files/pinboard.epub'));
  $output .= '</p>';
  
  $output .= '<p>';
  $output .= t('<a href="@link">Download the DITA zip file</a>', array('@link' => '/sites/default/files/dita.zip'));
  $output .= '</p>';
  
  $output .= '<p>';
  $output .= t('<a href="@link">Download the SCORM zip file</a>', array('@link' => '/sites/default/files/scorm.zip'));
  $output .= '</p>';
  
  
  // $output .= t('To see the version of the document transformed by OpenOffice go <a href="@link">here</a>', array('@link' => $file));
 
  if (!empty($return_url)) {
    $output .= '<p>';
    $output .= l(t('Return to previous page'), $return_url);
    $output .= '</p>';
  }
  return $output;
  

  

  
  
}



function template_preprocess_big_dipper(&$vars) {
  $vars['header'] = $vars['rows']['header'];
  $vars['body'] = $vars['rows']['body'];
  $vars['footer'] = $vars['rows']['footer'];

  $view     = $vars['view'];
  $fields   = &$view->field;
}

function template_preprocess_big_dipper_csv_header(&$vars) {
  _big_dipper_header_shared_preprocess($vars);

  // Make sure we catch saved options that are misspelled. LEGACY
  if (isset($vars['options']['seperator'])) {
    $vars['options']['separator'] = $vars['options']['seperator'];
  }
  // Support old misspelled templates. LEGACY
  $vars['seperator'] =
    $vars['separator'] = $vars['options']['separator'];

  // Special handling when quoted values are involved.
  if ($vars['options']['quote']) {
    $wrap = '"';
    $replace_value = '""';
  }
  else {
    $wrap = '';
    $replace_value = '';
  }

  // Format header values.
  foreach ($vars['header'] as $key => $value) {
    $output = decode_entities(strip_tags($value));
    if (!empty($vars['options']['trim'])) {
      $output = trim($output);
    }
    if (!empty($vars['options']['encoding']) && function_exists('iconv')) {
        switch($vars['options']['encoding']) {
          case 'ASCII':
            $converted = iconv("UTF-8", "ASCII//TRANSLIT", $output);
            if ($converted !== FALSE) {
              $output = $converted;
            }
            break;
        }
      }
      
    $vars['header'][$key] = $wrap . str_replace('"', $replace_value, $output) . $wrap;
  }
}

function template_preprocess_big_dipper_csv_body(&$vars) {
  _big_dipper_body_shared_preprocess($vars);

  // Make sure we catch saved options that are misspelled. LEGACY
  if (isset($vars['options']['seperator'])) {
    $vars['options']['separator'] = $vars['options']['seperator'];
  }
  // Support old misspelled templates. LEGACY
  $vars['seperator'] =
    $vars['separator'] = $vars['options']['separator'];

  // Special handling when quoted values are involved.
  if ($vars['options']['quote']) {
    $wrap = '"';
    $replace_value = '""';
  }
  else {
    $wrap = '';
    $replace_value = '';
  }

  // Format row values.
  foreach ($vars['themed_rows'] as $i => $values) {
    foreach ($values as $j => $value) {
      $output = decode_entities(strip_tags($value));
      if (!empty($vars['options']['trim'])) {
        $output = trim($output);
      }

      if (!empty($vars['options']['encoding']) && function_exists('iconv')) {
        switch($vars['options']['encoding']) {
          case 'ASCII':
            $converted = iconv("UTF-8", "ASCII//TRANSLIT", $output);
            if ($converted !== FALSE) {
              $output = $converted;
            }
            break;
        }
      }
      if (!empty($vars['options']['replace_newlines'])) {
        $output = str_replace("\n", $vars['options']['newline_replacement'], $output);
      }
      $vars['themed_rows'][$i][$j] = $wrap . str_replace('"', $replace_value, $output) . $wrap;
    }
  }
}

/**
 * Preprocess csv output template.
 */
function template_preprocess_big_dipper_csv(&$vars) {
  // TODO Replace items with themed_rows.
  _big_dipper_shared_preprocess($vars);

  // Make sure we catch saved options that are misspelled. LEGACY
  if (isset($vars['options']['separator'])) {
    $vars['options']['separator'] = $vars['options']['seperator'];
  }
  // Support old misspelled templates. LEGACY
  $vars['seperator'] =
    $vars['separator'] = $vars['options']['separator'];

  // Special handling when quoted values are involved.
  if ($vars['options']['quote']) {
    $wrap = '"';
    $replace_value = '""';
  }
  else {
    $wrap = '';
    $replace_value = '';
  }

  // Format header values.
  foreach ($vars['header'] as $key => $value) {
    $output = decode_entities(strip_tags($value));
    if ($vars['options']['trim']) {
      $output = trim($output);
    }
    if (!empty($vars['options']['encoding']) && function_exists('iconv')) {
      switch($vars['options']['encoding']) {
        case 'ASCII':
          $converted = iconv("UTF-8", "ASCII//TRANSLIT", $output);
          if ($converted !== FALSE) {
            $output = $converted;
          }
          break;
      }
    }
    $vars['header'][$key] = $wrap . str_replace('"', $replace_value, $output) . $wrap;
  }

  // Format row values.
  foreach ($vars['themed_rows'] as $i => $values) {
    foreach ($values as $j => $value) {
      $output = decode_entities(strip_tags($value));
      if ($vars['options']['trim']) {
        $output = trim($output);
      }
      if (!empty($vars['options']['encoding']) && function_exists('iconv')) {
        switch($vars['options']['encoding']) {
          case 'ASCII':
            $converted = iconv("UTF-8", "ASCII//TRANSLIT", $output);
            if ($converted !== FALSE) {
              $output = $converted;
            }
            break;
        }
      }
      $vars['themed_rows'][$i][$j] = $wrap . str_replace('"', $replace_value, $output) . $wrap;
    }
  }
}

/**
 * Preprocess txt output template.
 */
function template_preprocess_big_dipper_txt_body(&$vars) {
  _big_dipper_header_shared_preprocess($vars);
  _big_dipper_body_shared_preprocess($vars);
}

function template_preprocess_big_dipper_doc_body(&$vars) {
  // Pass through the generic MS Office preprocess.
  template_preprocess_big_dipper_msoffice_body($vars);
}

function template_preprocess_big_dipper_xls_body(&$vars) {
  // Pass through the generic MS Office preprocess.
  template_preprocess_big_dipper_msoffice_body($vars);
}

function template_preprocess_big_dipper_msoffice_body(&$vars) {
  _big_dipper_header_shared_preprocess($vars);
  _big_dipper_body_shared_preprocess($vars);

  $output = '';

  // Construct the tbody of a table, see theme_table().

  $ts = tablesort_init($vars['header']);
  
  $ts = '';

  $flip = array(
    'even' => 'odd',
    'odd' => 'even',
  );
  $class = 'even';
  foreach ($vars['themed_rows'] as $number => $row) {
    $attributes = array();

    // Check if we're dealing with a simple or complex row
    if (isset($row['data'])) {
      foreach ($row as $key => $value) {
        if ($key == 'data') {
          $cells = $value;
        }
        else {
          $attributes[$key] = $value;
        }
      }
    }
    else {
      $cells = $row;
    }
    if (count($cells)) {
      // Add odd/even class
      $class = $flip[$class];
      if (isset($attributes['class'])) {
        $attributes['class'] .= ' ' . $class;
      }
      else {
        $attributes['class'] = $class;
      }

      // Build row
      $output .= ' <section>';
      $i = 0;
      foreach ($cells as $cell) {
        $cell = tablesort_cell($cell, $vars['header'], $ts, $i++);
        
        // adding temporary field tags around the cell content for use as markers
        // this lets us distinguish cell top-level td tags from td tags inside tables that are, in turn, inside the cells
        $a = "<field>".$cell;
        $b = $a."</field>";
        $cell = $b;        
        $output .= _theme_table_cell($cell);
      }
      $output .= " </section>\n";
    }
  }
  
 // CSH: need to keep the inline tags
 // This gets rid of the table structure in the XML output of Views Data Export
 // Images are mapped to the cit site as a temporary expedient for testing
  
  $vars['tbody'] = preg_replace('/<td><field>/', '<subsection>',$output);
  
  $vars['tbody'] = preg_replace('/<\/field><\/td>/', '</subsection>',$vars['tbody']);
  
  $vars['tbody'] = preg_replace('/(<section><subsection>)/', '<section><h1>',$vars['tbody']);
  
  $vars['tbody'] = preg_replace('/(<\/subsection><subsection>)/', '</h1>',$vars['tbody']);
  
  $vars['tbody'] = preg_replace('/(<\/subsection>)/', '',$vars['tbody']);
  
  $vars['tbody'] = preg_replace('/src="\/sites/', 'src="http://cit.chatter.ie/sites',$vars['tbody']);
  
  $vars['tbody'] = preg_replace('/href="\/sites/', 'href="http://cit.chatter.ie/sites',$vars['tbody']);

}

function template_preprocess_big_dipper_doc_header(&$vars) {
  // Pass through the generic MS Office preprocess.
  template_preprocess_big_dipper_msoffice_header($vars);
}

function template_preprocess_big_dipper_xls_header(&$vars) {
  // Pass through the generic MS Office preprocess.
  template_preprocess_big_dipper_msoffice_header($vars);
}

function template_preprocess_big_dipper_msoffice_header(&$vars) {
  _big_dipper_header_shared_preprocess($vars);

  // Need to do a little work to construct the table header, see theme_table().
  $vars['header_row'] = '';
  $vars['header_row'] .= '<thead><tr>';

  $ts = tablesort_init($vars['header']);

  foreach ($vars['header'] as $cell) {
    $cell = tablesort_header($cell, $vars['header'], $ts);
    $vars['header_row'] .= _theme_table_cell($cell, TRUE);
  }

  $vars['header_row'] .= '</tr></thead>';

  $vars['header_row'] = preg_replace('/<\/?(a|span) ?.*?>/', '', $vars['header_row']); // strip 'a' and 'span' tags
  
  $vars['header_row'] = '';
}

/**
 * Preprocess xml output template.
 */
function template_preprocess_big_dipper_xml_header(&$vars) {
  // Compute the root XML node, using the base table, and appending an 's' if needed.
  $root_node = $vars['view']->base_table;
  if (rtrim($root_node, 's') == $root_node) {
    $root_node .= 's';
  }
  $vars['root_node'] = _big_dipper_xml_tag_clean($root_node);
}

/**
 * Preprocess xml output template.
 */
function template_preprocess_big_dipper_xml_footer(&$vars) {
  // Compute the root XML node, using the base table, and appending an 's' if needed.
  $root_node = $vars['view']->base_table;
  if (rtrim($root_node, 's') == $root_node) {
    $root_node .= 's';
  }
  $vars['root_node'] = _big_dipper_xml_tag_clean($root_node);
}

/**
 * Preprocess xml output template.
 */
function template_preprocess_big_dipper_xml_body(&$vars) {
  _big_dipper_header_shared_preprocess($vars);
  _big_dipper_body_shared_preprocess($vars);

  // Compute the tag name based on the views base table, minus any trailing 's'.
  $vars['item_node'] = _big_dipper_xml_tag_clean(rtrim($vars['view']->base_table, 's'));

  foreach ($vars['themed_rows'] as $num => $row) {
    foreach ($row as $field => $content) {
      // Prevent double encoding of the ampersand. Look for the entities produced by check_plain().
      $content = preg_replace('/&(?!(amp|quot|#039|lt|gt);)/', '&amp;', $content);
      // Convert < and > to HTML entities.
      $content = str_replace(
        array('<', '>'),
        array('&lt;', '&gt;'),
        $content);
      $vars['themed_rows'][$num][$field] = $content;
    }
  }

  foreach ($vars['header'] as $field => $header) {
    // If there is no field label, use 'no name'.
    $vars['xml_tag'][$field] = !empty($header) ? $header : 'no name';
    if ($vars['options']['transform']) {
      switch ($vars['options']['transform_type']) {
        case 'dash':
          $vars['xml_tag'][$field] = str_replace(' ', '-', $header);
          break;
        case 'underline':
          $vars['xml_tag'][$field] = str_replace(' ', '_', $header);
          break;
        case 'camel':
          $vars['xml_tag'][$field] = str_replace(' ', '', ucwords(strtolower($header)));
          // Convert the very first character of the string to lowercase.
          $vars['xml_tag'][$field][0] = strtolower($vars['xml_tag'][$field][0]);
          break;
        case 'pascal':
          $vars['xml_tag'][$field] = str_replace(' ', '', ucwords(strtolower($header)));
          break;
      }
    }
    // We should always try to output valid XML.
    $vars['xml_tag'][$field] = _big_dipper_xml_tag_clean($vars['xml_tag'][$field]);
  }
}

/**
 * Returns a valid XML tag formed from the given input.
 *
 * @param $tag The string that should be made into a valid XML tag.
 * @return The valid XML tag or an empty string if the string contained no valid
 * XML tag characters.
 */
function _big_dipper_xml_tag_clean($tag) {

  // This regex matches characters that are not valid in XML tags, and the
  // unicode ones that are. We don't bother with unicode, because it would so
  // the preg_replace down a lot.
  static $invalid_tag_chars_regex = '#[^\:A-Za-z_\-.0-9]+#';

  // These characters are not valid at the start of an XML tag:
  static $invalid_start_chars = '-.0123456789';

  // Convert invalid chars to '-':
  $tag = preg_replace($invalid_tag_chars_regex, '-', $tag);

  // Need to trim invalid characters from the start of the string:
  $tag = ltrim($tag, $invalid_start_chars);

  return $tag;
}

/**
 * Shared helper function for export preprocess functions.
 */
function _big_dipper_header_shared_preprocess(&$vars) {
  $view     = $vars['view'];
  $fields   = &$view->field;

  $vars['header'] = array();
  foreach ($fields as $key => $field) {
    if (empty($field->options['exclude'])) {
      $vars['header'][$key] = check_plain($field->label());
    }
  }

}

/**
 * Shared helper function for export preprocess functions.
 */
function _big_dipper_body_shared_preprocess(&$vars) {
  $view     = $vars['view'];
  $fields   = &$view->field;

  $rows = $vars['rows'];

  $vars['themed_rows'] = array();
  $keys = array_keys($fields);
  foreach ($rows as $num => $row) {
    $vars['themed_rows'][$num] = array();

    foreach ($keys as $id) {
      if (empty($fields[$id]->options['exclude'])) {
        $vars['themed_rows'][$num][$id] = $view->style_plugin->rendered_fields[$num][$id];
      }
    }
  }
}
