<?php
/**
 * @file
 * Views include file with views hooks.
 */

/**
 * Implementation of hook_views_plugins().
 */
function big_dipper_views_plugins() {
  $path = drupal_get_path('module', 'big_dipper');

  $style_defaults = array(
    'path' => $path . '/plugins',
    'parent' => 'big_dipper',
    'theme' => 'big_dipper',
    'theme path' => $path . '/theme',
    'theme file' => 'big_dipper.theme.inc',
    'uses row plugin' => FALSE,
    'uses fields' => TRUE,
    'uses options' => TRUE,
    'type' => 'data_export',
  );

  return array(
    'display' => array (
      'big_dipper' => array(
        'title' => t('Data export'),
        'help' => t('Export the view results to a file. Can handle very large result sets.'),
        'path' => $path . '/plugins',
        'handler' => 'big_dipper_plugin_display_export',
        'parent' => 'feed',
        'uses hook menu' => TRUE,
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'accept attachments' => FALSE,
        'admin' => t('Data export'),
        'help topic' => 'display-data-export',
      ),
    ),
    'style' => array(
      'big_dipper' => array(
        // this isn't really a display but is necessary so the file can
        // be included.
        'no ui' => TRUE,
        'handler' => 'big_dipper_plugin_style_export',
        'path' => $path . '/plugins',
        'theme path' => $path . '/theme',
        'theme file' => 'big_dipper.theme.inc',
        'type' => 'normal',
      ),
      'big_dipper_csv' => array(
        'title' => t('CSV file'),
        'help' => t('Display the view as a comma separated list.'),
        'handler' => 'big_dipper_plugin_style_export_csv',
        // Big Dipper element that will be used to set additional headers when serving the feed.
        'export headers' => array('Content-type' => 'text/csv; charset=utf-8'),
        // Big Dipper element mostly used for creating some additional classes and template names.
        'export feed type' => 'csv',
        'export feed text' => 'CSV',
        'export feed file' => '%view.csv',
        'export feed icon' => drupal_get_path('module', 'big_dipper') . '/images/csv.png',
        'additional themes' => array(
          'big_dipper_csv_header' => 'style',
          'big_dipper_csv_body' => 'style',
          'big_dipper_csv_footer' => 'style',
        ),
        'additional themes base' => 'big_dipper_csv',
      ) + $style_defaults,
      'big_dipper_doc' => array(
        'title' => t('DOC file'),
        'help' => t('Display the view as a doc file.'),
        'handler' => 'big_dipper_plugin_style_export',
        'export headers' => array('Content-Type' => 'application/msword'),
        'export feed type' => 'doc',
        'export feed text' => 'Word Document',
        'export feed file' => '%view.doc',
        'export feed icon' => drupal_get_path('module', 'big_dipper') . '/images/doc.png',
        'additional themes' => array(
          'big_dipper_doc_header' => 'style',
          'big_dipper_doc_body' => 'style',
          'big_dipper_doc_footer' => 'style',
        ),
        'additional themes base' => 'big_dipper_doc',
      ) + $style_defaults,
      'big_dipper_txt' => array(
        'title' => t('TXT file'),
        'help' => t('Display the view as a txt file.'),
        'handler' => 'big_dipper_plugin_style_export',
        'export headers' => array('Content-Type' => 'text/plain'),
        'export feed type' => 'txt',
        'export feed text' => 'Plain Text Document',
        'export feed file' => '%view.txt',
        'export feed icon' => drupal_get_path('module', 'big_dipper') . '/images/txt.png',
        'additional themes' => array(
          'big_dipper_txt_header' => 'style',
          'big_dipper_txt_body' => 'style',
          'big_dipper_txt_footer' => 'style',
        ),
        'additional themes base' => 'big_dipper_txt',
      ) + $style_defaults,
      'big_dipper_xls' => array(
        'title' => t('XLS file'),
        'help' => t('Display the view as a xls file.'),
        'handler' => 'big_dipper_plugin_style_export',
        'export headers' => array('Content-Type' => 'application/vnd.ms-excel'),
        'export feed type' => 'xls',
        'export feed text' => 'XLS',
        'export feed file' => '%view.xls',
        'export feed icon' => drupal_get_path('module', 'big_dipper') . '/images/xls.png',
        'additional themes' => array(
          'big_dipper_xls_header' => 'style',
          'big_dipper_xls_body' => 'style',
          'big_dipper_xls_footer' => 'style',
        ),
        'additional themes base' => 'big_dipper_xls',
      ) + $style_defaults,
      'big_dipper_xml' => array(
        'title' => t('XML file'),
        'help' => t('Display the view as a txt file.'),
        'handler' => 'big_dipper_plugin_style_export_xml',
        'export headers' => array('Content-Type' => 'text/xml'),
        'export feed type' => 'xml',
        'export feed text' => 'XML',
        'export feed file' => '%view.xml',
        'export feed icon' => drupal_get_path('module', 'big_dipper') . '/images/xml.png',
        'additional themes' => array(
          'big_dipper_xml_header' => 'style',
          'big_dipper_xml_body' => 'style',
          'big_dipper_xml_footer' => 'style',
        ),
        'additional themes base' => 'big_dipper_xml',
      ) + $style_defaults,
    ),
  );
}
